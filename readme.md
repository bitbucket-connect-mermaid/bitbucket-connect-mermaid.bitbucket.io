# Bitbucket Connect X MermaidJS File Viewer Addon

Connect addon to enable rich rendering of MermaidJS diagrams

![Example](images/example.png)

## Installing

1. Navigate to the settings page of the Workspace that you wish to install the addon in - https://bitbucket.org/{YourWorkspaceName}/workspace/settings/addon-management

2. Under the "Apps and Features" section, click "Marketplace"

3. Scroll down until you see "Mermaid Diagrams FileViewer", click add, and follow on-screen prompts


## Using

Once the addon has been installed into the target workspace, all files with the extension ".mmd" in that Workspace's repos will have the option to select "Mermaid" from the list of file viewers. Simply select the approriate file viewer to see the rendered results.

![File Viewers Dropdown](images/file_viewers_dropdown.png)
